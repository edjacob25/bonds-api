using System.Text.RegularExpressions;
using Xunit;

namespace BondsApi.Test
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("", false)]
        [InlineData("1", true)]
        [InlineData("1.2", true)]
        [InlineData("1.1234", true)]
        [InlineData("1.12345", false)]
        [InlineData("1.123m", false)]
        [InlineData("1.", true)]
        [InlineData("1.0000", true)]
        [InlineData("0.00000000", false)]
        public void DecimalRegexTest(string match, bool expected)
        {
            var regex = new Regex(@"^\d+(\.\d{0,4})?$");
            Assert.Equal(expected, regex.IsMatch(match));
        }
    }
}