using System.ComponentModel.DataAnnotations;

namespace BondsApi.Models.DTO
{
    public record SellRequest(
        [MaxLength(40)] [MinLength(3)] [RegularExpression(@"(\w|\d)+")]
        string Name,
        [Range(1, 10_000)] int NumberOfBonds,
        [Range(0.0f, 100_000_000_000)]
        [RegularExpression(@"^\d+(\.\d{0,4})?$", ErrorMessage = "Cannot have more than 4 decimals")]
        decimal Price);

    public record Bond(string Name, int Quantity, decimal Price, bool IsSold, string Currency = "MXN");
    public record UnauthorizedResponse(string Reason);
}