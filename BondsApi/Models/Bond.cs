using Newtonsoft.Json;

namespace BondsApi.Models
{
    public class Bond
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }

        public bool IsSold => BuyerId != null;
        
        public long SellerId { get; set; }
        [JsonIgnore]
        public User Seller { get; set; }
        public long? BuyerId { get; set; }
        [JsonIgnore]
        public User? Buyer { get; set; }
    }
}