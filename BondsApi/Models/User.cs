using System.Collections.Generic;

namespace BondsApi.Models
{
    public class User
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ApiKey { get; set; }
        
        public IList<Bond> Postings { get; set; }
        public IList<Bond> Purchases { get; set; }
    }
}