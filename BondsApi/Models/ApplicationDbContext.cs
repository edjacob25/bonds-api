using Microsoft.EntityFrameworkCore;

namespace BondsApi.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Bond> Bonds { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Bond>().ToTable("Bond");
            builder.Entity<Bond>().HasKey(e => e.Id);
            builder.Entity<Bond>().HasIndex(e => e.Name).IsUnique();
            builder.Entity<Bond>()
                .HasOne(e => e.Seller)
                .WithMany(e => e.Postings);
            builder.Entity<Bond>()
                .HasOne(e => e.Buyer)
                .WithMany(e => e.Purchases);
            builder.Entity<Bond>().Property(e => e.Price)
                .HasPrecision(12, 4);

            builder.Entity<User>().ToTable("User");
            builder.Entity<User>().HasKey(e => e.Id);
            builder.Entity<User>().HasIndex(e => e.ApiKey).IsUnique();
        }
    }
}