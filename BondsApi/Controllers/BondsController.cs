using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BondsApi.Models;
using BondsApi.Models.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using BondDB = BondsApi.Models.Bond;
using BondDTO = BondsApi.Models.DTO.Bond;

namespace BondsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(UnauthorizedResponse))]
    [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(IDictionary<string, string>))]
    public class BondsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IHttpClientFactory _factory;
        private readonly ILogger<BondsController> _logger;

        public BondsController(ApplicationDbContext context, IHttpClientFactory factory,
            ILogger<BondsController> logger)
        {
            _context = context;
            _factory = factory;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<BondDTO>))]
        public async Task<ActionResult<IEnumerable<BondDTO>>> GetBonds(bool usd)
        {
            var bonds = await _context.Bonds.Where(e => e.BuyerId == null)
                .Select(e => BondToBondDto(e)).ToListAsync();
            if (!usd) return bonds;

            var client = _factory.CreateClient("BMX");
            var response = await client.GetStringAsync("/SieAPIRest/service/v1/series/SF43718/datos/oportuno");
            var changeRatio = (float?)JObject.Parse(response)["bmx"]?["series"]?[0]?["datos"]?[0]?["dato"]
                ?.ToObject(typeof(float)) ?? 1.0f;
            bonds = bonds.Select(e =>
                e with
                {
                    Price = Math.Round(e.Price / (decimal)changeRatio, 4),
                    Currency = "USD"
                }
            ).ToList();

            return bonds;
        }

        // GET: api/Bonds/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BondDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<BondDTO>> GetBond(long id)
        {
            var bond = await _context.Bonds.FindAsync(id);

            if (bond == null)
            {
                return NotFound();
            }

            return BondToBondDto(bond);
        }

        [HttpPost("sell")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult<BondDTO>> PostBond(SellRequest request)
        {
            var apikey = Request.Headers["apikey"][0];
            var user = await _context.Users.SingleAsync(e => e.ApiKey == apikey);
            var bond = new BondDB
            {
                Name = request.Name,
                Quantity = request.NumberOfBonds,
                Price = request.Price,
                Seller = user
            };
            _context.Bonds.Add(bond);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBond", new { id = bond.Id }, BondToBondDto(bond));
        }

        [HttpPost("buy/{id}")]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(BondDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<BondDTO>> BuyBond(long id)
        {
            var bond = await _context.Bonds.FindAsync(id);
            if (bond == null) return NotFound();
            if (bond.IsSold) return BadRequest("Bond already sold");
            var apikey = Request.Headers["apikey"][0];
            var user = await _context.Users.SingleAsync(e => e.ApiKey == apikey);
            bond.Buyer = user;
            await _context.SaveChangesAsync();
            return AcceptedAtAction("GetBond", new { id = bond.Id }, BondToBondDto(bond));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteBond(long id)
        {
            var bond = await _context.Bonds.FindAsync(id);
            if (bond == null)
            {
                return NotFound();
            }

            var apikey = Request.Headers["apikey"][0];
            var user = await _context.Users.SingleAsync(e => e.ApiKey == apikey);
            if (bond.Seller != user) return BadRequest("The seller of the bond is not the one requesting it");
            _context.Bonds.Remove(bond);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool BondExists(long id)
        {
            return _context.Bonds.Any(e => e.Id == id);
        }

        private static BondDTO BondToBondDto(BondDB bond) => new(bond.Name, bond.Quantity, bond.Price, bond.IsSold);
    }
}