﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BondsApi.Migrations
{
    public partial class Adduniqueindices : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_User_ApiKey",
                table: "User",
                column: "ApiKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Bond_Name",
                table: "Bond",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_User_ApiKey",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_Bond_Name",
                table: "Bond");
        }
    }
}
