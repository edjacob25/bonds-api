using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using BondsApi.Models;
using BondsApi.Models.DTO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace BondsApi.Middleware
{
    public class AuthMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, ApplicationDbContext db)
        {
            if (context.Request.Headers.TryGetValue("apikey", out var apikey))
            {
                if (db.Users.Any(e => e.ApiKey == apikey[0]))
                {
                    // Continue the request
                    await _next(context);
                }
                else
                {
                    await ReturnUnauthorized(context, "Invalid ApiKey");
                }
            }
            else
            {
                await ReturnUnauthorized(context, "Api Key not present");
            }
        }

        private async Task ReturnUnauthorized(HttpContext context, string reason)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            var body = new UnauthorizedResponse(reason);
            await context.Response.WriteAsync(JsonConvert.SerializeObject(body));
            await context.Response.StartAsync();
        }
    }

    public static class AuthMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthMiddleware(
            this IApplicationBuilder builder) =>
            builder.UseMiddleware<AuthMiddleware>();
    }
}