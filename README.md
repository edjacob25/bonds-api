# Bonds buying API

This is a simple restful API which exposes a couple of endpoints related to bond buying and selling. It is extremely simplistic and it is more useful to showcase the possibilities of the API.

## Architecture
The application uses just a few endpoints, but the most important for discoverability is `/swagger/index.html`, which is the swagger UI, in which the rest of the endpoints can be seen.

All the application endpoints are behind a simple api key authorization, which has to be configured beforehand in the client. This is made by setting a custom header with the format `apikey:apikeytext` in each request. At the moment, this Api-key is set once per user and has to be added manually to the database when setting a new user. As this api-key is unique, we can identify the user and set the appropriate data when receiving requests.

Right now, for the ease of use, we are using a `Sqlite` database now, but it can be easily changed to a more production ready database, such as `PostgreSql` or `SqlServer`.
The application is rate limited at 1000 requests/minute per ip connected to it.

## Running
Install the `dotnet 5 sdk` from [here](https://dotnet.microsoft.com/download/dotnet/5.0).

Clone this repository
```bash
git clone https://gitlab.com/edjacob25/bonds-api.git
```
Get into the directory of the project and restore the projects
```bash
cd bonds-api
dotnet restore
```
Get into the api project and create the database
```bash
cd BondsApi
dotnet ef database update
```
Init the secret store with the apiKey for the Banco de Mexico API
```bash
dotnet user-secrets init
dotnet user-secrets set "BDM_ApiKey" "apikeyofBancoDeMexico"
```
Insert a couple of users into the DB
```bash
sqlite3 app.db
```
```sqlite
INSERT INTO User (Name, ApiKey) VALUES ("User1", "ApiKeyOfUser1");
INSERT INTO User (Name, ApiKey) VALUES ("User2", "ApiKeyOfUser2");
```

Then you can start the project
```bash
dotnet run
```
For easiness, you can test using the swagger UI included at `/swagger/index.html`, but you can also use `curl` or `http` to test it, for example.
```bash
curl -X POST "https://localhost:5000/api/Bonds/sell" -H  "accept: text/json" -H  "apikey: apiKey1" -H  "Content-Type: application/json" -d "{\"name\":\"nameofthebond\",\"numberOfBonds\":5,\"price\":1.2345}"
```
## To-do
- More testing
- More endpoints with more functionality